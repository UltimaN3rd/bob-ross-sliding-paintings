shader_type canvas_item;
uniform vec2 texture_size = vec2(400.0, 150.0);
uniform int outline_thickness = 3;

void fragment(){
	COLOR = texture(TEXTURE,UV);
	if(texture(TEXTURE,UV).a <= 0.1){
		for(int i=-outline_thickness; i<outline_thickness+1; i++){
			for(int j=-outline_thickness; j<outline_thickness+1; j++){
				if(texture(TEXTURE,UV+vec2(float(i)/texture_size.x,float(j)/texture_size.y)).a != 0.0){
					COLOR = vec4(0,0,0,1);
					break;
				}
			}
		}
	}
}