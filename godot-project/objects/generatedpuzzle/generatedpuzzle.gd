tool

extends StaticBody

const block_visual_scene = preload("res://models/block/block.dae")
const button_scene = preload("res://models/button/button.tscn")
const label_scene = preload("res://objects/3dlabel.tscn")

export(int) var blocks_x = 3
export(int) var blocks_y = 3
export(bool) var rebuild = false setget rebuild_pressed
export(Texture) var texture = preload("res://paintings/bubbling_stream.jpg")
export(Texture) var texture_n = preload("res://paintings/bubbling_stream_n.png")
export(String, FILE) var texture_path
export(String, FILE) var texture_n_path
export(String) var painting_name = ""
export(bool) var load_saved_arrangement = false
export(String) var saved_sequence_string = ""

signal solved(name)

var blocks = []
var original_arrangement = []
var numblocks = 0
var children = []
var numrandomsleft = 100
var lastblock = null
var texratio = 1
var empty_block = Vector2()
var slide_time = 0.3
var saved_sequence
var move_sequence = ""

onready var resetbutton = button_scene.instance()
onready var randomizebutton = button_scene.instance()
onready var camerabutton = button_scene.instance()
onready var randomertimer = Timer.new()
onready var preview_block = MeshInstance.new()
onready var preview_button = button_scene.instance()
onready var print_sequence_button = button_scene.instance()
onready var apply_saved_arrangement_button = button_scene.instance()

func _ready():
	connect("solved", get_parent().get_parent(), "solved")
	randomize()
	texture_path = "res://paintings/" + name + ".jpg"
	texture_n_path = "res://paintings/" + name + "_n.png"
	painting_name = name
	painting_name = painting_name.replace('_', ' ')
	painting_name = painting_name.capitalize()
	if texture_path != null:
		texture = load(texture_path)
	if texture_n_path != null:
		texture_n = load(texture_n_path)
	texratio = texture.get_size().x/texture.get_size().y
	build()
	$label.translation.y = 1.7/texratio
	$label.text = painting_name
	randomertimer.wait_time = 1.0 / blocks_x / blocks_y
	randomertimer.autostart = false
	randomertimer.connect("timeout", self, "move_random_block")
	add_child(randomertimer)
	resetbutton.translation = Vector3(-2.2, -1, 0.3)
	resetbutton.rotation.y = 0.5
	resetbutton.text = "Reset"
	resetbutton.connect("pressed", self, "reset")
	add_child(resetbutton)
	randomizebutton.translation = Vector3(-2.2, -.4, 0.3)
	randomizebutton.rotation.y = 0.5
	randomizebutton.text = "Randomize"
	randomizebutton.connect("pressed", self, "randomize_blocks")
	add_child(randomizebutton)
	camerabutton.translation = Vector3(-2.2, .2, 0.3)
	camerabutton.rotation.y = 0.5
	camerabutton.text = "Focus\nview"
	camerabutton.toggle = true
	camerabutton.connect("pressed", $"../../player/Camera", "cam_button", [self])
	add_child(camerabutton)
	slide_time = 3.0 / blocks_x / blocks_y
	preview_block.mesh = PlaneMesh.new()
	preview_block.mesh.size = Vector2(3, 3.0/texratio)
	preview_block.translation = Vector3(0,0,.212)
	preview_block.rotation.x = -PI/2
	preview_block.rotation.y = PI
	preview_block.mesh.material = SpatialMaterial.new()
	preview_block.mesh.material.albedo_texture = texture
	preview_block.mesh.material.flags_unshaded = true
	preview_block.visible = false
	add_child(preview_block)
	preview_button.translation = Vector3(-2.2, 0.8, 0.3)
	preview_button.rotation.y = 0.5
	preview_button.text = "Preview"
	preview_button.toggle = true
	preview_button.connect("pressed", self, "toggle_preview")
	add_child(preview_button)
#	print_sequence_button.translation = Vector3(-2.6, 0, 0.1)
#	print_sequence_button.text = "Print\nsequence"
#	print_sequence_button.connect("pressed", self, "print_sequence_button_pressed")
#	add_child(print_sequence_button)
#	apply_saved_arrangement_button.translation = Vector3(-2.6, 0.6, 0.1)
#	apply_saved_arrangement_button.text = "Apply saved\narrangement"
#	apply_saved_arrangement_button.connect("pressed", self, "apply_saved_arrangement_button_pressed")
#	add_child(apply_saved_arrangement_button)
	$light.translation.y = 1.5/texratio + 0.025
	$light2.translation.y = -1.5/texratio - 0.025
	var shapeowner = create_shape_owner(self)
	var colshape = BoxShape.new()
	colshape.extents = Vector3(1.87, 1.87/texratio, 0.34)
	shape_owner_add_shape(shapeowner, colshape)
#	var visualshape = MeshInstance.new()
#	visualshape.mesh = CubeMesh.new()
#	visualshape.mesh.size = Vector3(1.87, 1.87/texratio, 0.34)*2
#	visualshape.translation = Vector3(0,0,-0.21)
#	add_child(visualshape)
	if not Engine.is_editor_hint():
		if load_saved_arrangement:
			apply_saved_arrangement_button_pressed()
		else:
			randomize_instant()

func toggle_preview():
	preview_block.visible = not preview_block.visible

func build():
	if Engine.editor_hint:
		texratio = texture.get_size().x/texture.get_size().y
	for child in get_children():
		if not child.is_in_group("prebuilt"):
			child.queue_free()
	call_deferred("$frame.set_scale", Vector3(1,1/texratio,1))
	blocks = []
	original_arrangement = []
	for y in range(blocks_y):
		blocks.append([])
		original_arrangement.append([])
		for x in range(blocks_x):
			var newblock = StaticBody.new()
			newblock.add_to_group("pressable")
			newblock.add_to_group("blocks")
			newblock.set_script(load("res://block.gd"))
			var blockmesh = block_visual_scene.instance()
			var newmat = SpatialMaterial.new()
			newmat.albedo_texture = texture
			newmat.flags_unshaded = true
			newmat.uv1_offset = Vector3(float(x)/float(blocks_x), float(y)/float(blocks_y), 0)
			newmat.uv1_scale = Vector3(1.0/blocks_x, 1.0/blocks_y,1)
			newmat.metallic = 0
			newmat.metallic_specular = 0
			newmat.normal_enabled = true
			newmat.normal_texture = texture_n
			newmat.normal_scale = 0.5
			blockmesh.get_child(0).set_surface_material(0, newmat)
			blockmesh.scale = Vector3(1.0/blocks_x, 1.0/blocks_y/texratio, 1)
			blockmesh.translation = Vector3(0, 0, 0.15)
			var shapeowner = newblock.create_shape_owner(newblock)
			var colshape = BoxShape.new()
			colshape.extents = Vector3(1.5/blocks_x, 1.5/texratio/blocks_y, 0.15)
			newblock.shape_owner_add_shape(shapeowner, colshape)
			newblock.add_child(blockmesh)
			newblock.translation = Vector3(-1.5+1.5/blocks_x+3.0*x/blocks_x, 1.5/texratio-1.5/texratio/blocks_y-3.0/texratio*y/blocks_y, 0.06)
			blocks[y].append(newblock)
			original_arrangement[y].append(newblock)
	blocks[blocks_y-1][blocks_x-1] = null
	original_arrangement[blocks_y-1][blocks_x-1] = null
	empty_block = Vector2(blocks_x-1, blocks_y-1)
	numblocks = blocks_y*blocks_x-1
	var x = 0
	var y = 0
	for blockline in blocks:
		for block in blockline:
			if block != null:
				add_child(block)
			x += 1
		y += 1

func rebuild_pressed(new_rebuild):
	build()

func reset():
	randomertimer.stop()
	for y in range(blocks_y):
		for x in range(blocks_x):
			blocks[y][x] = original_arrangement[y][x]
			if blocks[y][x] != null:
				blocks[y][x].translation = Vector3(-1.5+1.5/blocks_x+3.0*x/blocks_x, 1.5/texratio-1.5/texratio/blocks_y-3.0/texratio*y/blocks_y, 0.06)
	reset_block_positions()
	$solved_indicator.material_override.emission = Color(0,1,0)
	$solved_indicator.material_override.albedo_color = Color(0,1,0)
	empty_block = Vector2(blocks_x-1, blocks_y-1)

func randomize_blocks():
	numrandomsleft = blocks_x*blocks_y*10
	randomertimer.start()

func randomize_instant():
	for i in range(blocks_x * blocks_y * 10):
		move_random_block()
	reset_block_positions()

func move_random_block():
	numrandomsleft -= 1
	var thisblock = lastblock
	var randblock = randi()%4
	while thisblock == lastblock:
		if randblock == 0 and empty_block.x > 0: #left
			thisblock = blocks[empty_block.y][empty_block.x-1]
		elif randblock == 1 and empty_block.x < blocks_x-1: #right
			thisblock = blocks[empty_block.y][empty_block.x+1]
		elif randblock == 2 and empty_block.y > 0: #top
			thisblock = blocks[empty_block.y-1][empty_block.x]
		elif randblock == 3 and empty_block.y < blocks_y-1: #bottom
			thisblock = blocks[empty_block.y+1][empty_block.x]
		randblock = (randblock+1)%4
	lastblock = thisblock
	move_block(thisblock)
	if numrandomsleft <= 0:
		randomertimer.stop()

func move_block_index(_index):
	move_block(blocks[_index.y][_index.x])

func move_block(block):
#	var block_curpos = Vector2()
#	for y in range(blocks_y):
#		for x in range(blocks_x):
#			if blocks[y][x] == block:
#				block_curpos=Vector2(x,y)
#			elif blocks[y][x] == null:
#				open_pos = Vector2(x,y)
	var block_curpos = Vector2(floor((block.translation.x+1.5 - 1.5/blocks_x) / 3.0*blocks_x + 0.0001), floor((block.translation.y - 1.5/texratio) / -3.0*texratio*blocks_y + 0.0001))
	if empty_block.x == block_curpos.x and (empty_block.y == block_curpos.y-1 or empty_block.y == block_curpos.y+1) or empty_block.y == block_curpos.y and (empty_block.x == block_curpos.x-1 or empty_block.x == block_curpos.x+1):
		block.translation = Vector3(-1.5+1.5/blocks_x+3.0*empty_block.x/blocks_x, 1.5/texratio-1.5/texratio/blocks_y-3.0/texratio*empty_block.y/blocks_y, 0.06)
		blocks[empty_block.y][empty_block.x] = block
		blocks[block_curpos.y][block_curpos.x] = null
		blocks[empty_block.y][empty_block.x].animate_move_from(Vector2(3.0/blocks_x*(block_curpos.x-empty_block.x),3.0/texratio/blocks_y*(empty_block.y-block_curpos.y)), slide_time)
		empty_block = block_curpos
		move_sequence += String(block_curpos.x) + "," + String(block_curpos.y) + ","
		if blocks == original_arrangement:
			$solved_indicator.material_override.emission = Color(0,1,0)
			$solved_indicator.material_override.albedo_color = Color(0,1,0)
			emit_signal("solved", name)
		else:
			$solved_indicator.material_override.emission = Color(1,0,0)
			$solved_indicator.material_override.albedo_color = Color(1,0,0)
		return true
	return false
#	var blockstring = ""
#	for y in range(3):
#		for x in range(3):
#			if block_points[y][x] == null:
#				blockstring += "null "
#			else:
#				blockstring += block_points[y][x].name + " "
#		blockstring += "\n"
#	print(blockstring)

#func print_sequence_button_pressed():
#	print(move_sequence)

func apply_saved_arrangement_button_pressed():
	var splitstring = saved_sequence_string.split(",")
	saved_sequence = []
	for i in range(splitstring.size() / 2):
		saved_sequence.append(Vector2(splitstring[i*2], splitstring[i*2+1]))
	for move in saved_sequence:
		move_block_index(move)
	reset_block_positions()

func reset_block_positions():
	for y in range(blocks_y):
		for x in range(blocks_x):
			if blocks[y][x] != null:
				blocks[y][x].animate_move_from(Vector2())
