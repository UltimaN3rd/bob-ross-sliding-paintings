extends KinematicBody

const MAX_SPEED = 2
const JUMP_SPEED = 4
const ACCEL= 3
const DEACCEL= 10
const MAX_SLOPE_ANGLE = 30

var g = -5
var vel = Vector3()
onready var animation_player = $AnimationPlayer
onready var state = frozen.new(self) setget change_state
onready var unfreeze_timer = Timer.new()

func change_state(_state):
	state.delete()
	state = _state

func _physics_process(delta):
	state._physics_process(delta)

func _process(delta):
	state._process(delta)

func pellet_added():
	change_state(chasing_pellet.new(self))

class state:
	var me
	func _init():
		pass
	func delete():
		pass
	func _physics_process(delta):
		pass
	func _process(delta):
		pass

class chasing_pellet extends state:
	var target_pellet
	func _init(_me):
		me = _me
		me.animation_player.play("walk")
		target_pellet = me.get_node("../pellets").get_child(me.get_node("../pellets").get_child_count()-1)
	
	func _physics_process(delta):
		var target_dist = me.translation.distance_to(target_pellet.translation)
		if target_dist < 0.3:
			target_pellet.queue_free()
			if me.get_node("../pellets").get_child_count() <= 1:
				me.state = me.stopped.new(me)
			else:
				target_pellet = me.get_node("../pellets").get_child(me.get_node("../pellets").get_child_count()-2)
			return
		var target_rot = -(Vector2(me.translation.x, me.translation.z).angle_to_point(Vector2(target_pellet.translation.x, target_pellet.translation.z))) - PI / 2.0
		if abs(target_rot - me.rotation.y) > 0.2:
			var dir = sign(target_rot - me.rotation.y)
			if abs(target_rot - me.rotation.y) > PI:
				dir = -dir
			me.rotation.y += dir * delta * 20.0 * min(1, target_dist)
		var dir = Vector3(0, 0, 1).rotated(Vector3(0,1,0), me.rotation.y)
		dir = dir.normalized()
		me.vel.y += delta*me.g

		var hvel = me.vel
		hvel.y = 0

		var target = dir*me.MAX_SPEED * 2.0
		var accel = 0
		if (dir.dot(hvel) > 0):
			accel = me.ACCEL * 2.0
		else:
			accel = me.DEACCEL

		hvel = hvel.linear_interpolate(target, accel*delta)

		me.vel.x = hvel.x
		me.vel.z = hvel.z

		me.vel = me.move_and_slide(me.vel,Vector3(0,1,0))

class walking extends state:
	var jumptimer
	var turntimer

	var rot = 0

	func _init(_me):
		me = _me
		jumptimer = Timer.new()
		jumptimer.wait_time = rand_range(1, 10)
		me.add_child(jumptimer)
		jumptimer.connect("timeout", self, "jump")
		jumptimer.start()
		turntimer = Timer.new()
		turntimer.wait_time = rand_range(0.1, 3)
		me.add_child(turntimer)
		turntimer.connect("timeout", self, "turn")
		turntimer.start()
		me.animation_player.play("walk")

	func delete():
		jumptimer.queue_free()
		turntimer.queue_free()

	func _physics_process(delta):
		if abs(rot) > 0.05:
			var rotdif = rot
			rot = lerp(rot, 0, delta*10.0)
			rotdif -= rot
			me.rotation.y += rotdif
			rot -= rotdif
		var dir = Vector3(0, 0, 1).rotated(Vector3(0,1,0), me.rotation.y)
		dir = dir.normalized()
		me.vel.y += delta*me.g

		var hvel = me.vel
		hvel.y = 0

		var target = dir*me.MAX_SPEED
		var accel = 0
		if (dir.dot(hvel) > 0):
			accel = me.ACCEL
		else:
			accel = me.DEACCEL

		hvel = hvel.linear_interpolate(target, accel*delta)

		me.vel.x = hvel.x
		me.vel.z = hvel.z

		me.vel = me.move_and_slide(me.vel,Vector3(0,1,0))

		if me.is_on_floor():
			if me.animation_player.current_animation == "fly":
				me.animation_player.play("walk")
		else:
			if me.animation_player.current_animation == "walk":
				me.animation_player.play("fly")

	func _process(delta):
		if randf()>0.999:
			me.state = me.stopped.new(me)

	func jump():
		#if me.is_on_floor():
		me.vel.y = me.JUMP_SPEED
		jumptimer.wait_time = rand_range(5, 30)
		jumptimer.start()

	func turn():
		rot = rand_range(-PI, PI)
		turntimer.wait_time = rand_range(0.1, 1)
		turntimer.start()

class stopped extends state:
	func _init(_me):
		me = _me
		me.animation_player.stop(true)

	func _process(delta):
		if randf()>0.995:
			me.state = me.walking.new(me)

	func _physics_process(delta):
		me.vel.y += delta*me.g

		var hvel = me.vel
		hvel.y = 0
		hvel = hvel.linear_interpolate(Vector3(), me.DEACCEL*delta)
		me.vel.x = hvel.x
		me.vel.z = hvel.z

		me.vel = me.move_and_slide(me.vel,Vector3(0,1,0))

class frozen extends state:
	func _init(_me):
		me = _me
		me.animation_player.stop()
		me.visible = false

	func delete():
		me.visible = true
		me.get_node("AudioStreamPlayer3D").start_chirping()

func unfreeze():
	change_state(walking.new(self))