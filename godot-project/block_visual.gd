extends MeshInstance

const time_to_slide = 0.3

var start_pos = Vector2()
var slide_time = 0

func _ready():
	set_process(false)

func animate_move_from(from):
	start_pos = from
	slide_time = 0
	set_process(true)

func _process(delta):
	slide_time += delta
	translation.x = start_pos.x * (1 - slide_time/time_to_slide)
	translation.y = start_pos.y * (1 - slide_time/time_to_slide)
	if(slide_time >= time_to_slide):
		translation = Vector3()
		set_process(false)