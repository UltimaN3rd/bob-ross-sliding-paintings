extends Node

onready var menu = $menu

var completed_all_puzzles = false

var save_data = {
	"arizona_splendor" : false,
	"autumn_glory" : false,
	"bubbling_stream" : false,
	"camp_fire" : false,
	"distant_hills" : false,
	"mountain_cabin" : false,
	"mountain_retreat" : false,
	"mountain_waterfall" : false,
	"secluded_lake" : false,
	"the_old_mill" : false,
}

var extra_data = {
	"music":100,
	"sfx":100,
	"screenmode":"Windowed",
	"fov":90
}

func _ready():
	load_game()

func save_game():
	var save_game = File.new()
	save_game.open("user://save_game.save", File.WRITE)
	save_game.store_line(to_json(save_data))
	save_game.store_line(to_json(extra_data))
	save_game.close()

func load_game():
	var load_game = File.new()
	if not load_game.file_exists("user://save_game.save"):
		return
	load_game.open("user://save_game.save", File.READ)
	var current_line = parse_json(load_game.get_line())
	for i in current_line.keys():
		save_data[i] = current_line[i]
	current_line = parse_json(load_game.get_line())
	for i in current_line.keys():
		extra_data[i] = current_line[i]
	load_game.close()
	for i in save_data.keys():
		if save_data[i]:
			get_node("puzzles/" + i).reset()
	$menu/options/music_volume_label/music_slider.value = extra_data["music"]
	$menu/options/effects_volume_label/effects_slider.value = extra_data["sfx"]
	$menu/options.screenmode = extra_data["screenmode"]
	$player/Camera.fov = extra_data["fov"]
	$menu/options/fov_label/fov_slider.value = extra_data["fov"] / 180
	if extra_data["screenmode"] == "Windowed":
		$menu/options._on_button_windowed_pressed()
	elif extra_data["screenmode"] == "Borderless":
		$menu/options._on_button_borderless_pressed()
	else: #"Fullscreen"
		$menu/options._on_button_fullscreen_pressed()
	check_unfreeze_pekin()

func solved(name):
	save_data[name] = true
	save_game()
	check_unfreeze_pekin()

func check_unfreeze_pekin():
	for i in save_data.values():
		if not i:
			return
	completed_all_puzzles = true
	$world/pekin.unfreeze()
	$world/table/bookstand/book/pages.mesh.surface_get_material(0).albedo_texture = load("res://models/book/tex_page_b.png")

func show_menu():
	add_child(menu)
	menu.enter_menu()

func hide_menu():
	remove_child(menu)