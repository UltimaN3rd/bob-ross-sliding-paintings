extends StaticBody

export(Color) var color = Color()

const time_to_slide = 0.3

var start_pos = Vector2()
var slide_time = 0
var mesh

func _ready():
	for child in get_children():
		if child is Spatial:
			mesh = child
	set_process(false)

func pressed():
	get_parent().move_block(self)

func animate_move_from(from, time = 0.3):
	start_pos = from
	slide_time = 0
	time_to_slide = time
	set_process(true)

func _process(delta):
	slide_time += delta
	mesh.translation.x = start_pos.x * (1 - slide_time/time_to_slide)
	mesh.translation.y = start_pos.y * (1 - slide_time/time_to_slide)
	if(slide_time >= time_to_slide):
		mesh.translation.x = 0
		mesh.translation.y = 0
		set_process(false)